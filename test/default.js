var _ = require('underscore');
var request = require('supertest');
var appRoot = require('app-root-path');
var should = require('chai').should();

var app = require(appRoot + '/source/server.js').app;

describe('default', function() {

  it('GET /', function(done) {
    request(app)
      .get('/')
      .set('Accept', 'application/json')
      .expect(200, done);
  });

  it('GET /search', function(done) {
    request(app)
      .get('/search')
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, results) {
        if (err) return done(err);

        // for SHA ending with number, color should be set
        // for SHA not ending with number, color should not be set
        var colorTest = _.every(results.body, function(item) {
          if(!isNaN(item.sha.slice(-1))) {
            return (item.color === '#DC143C');
          } else {
            if(isNaN(item.sha.slice(-1))) {
              return (item.color === undefined)
            }
          }
        });

        colorTest.should.be.eql(true);
        done();
      });
  });

});
