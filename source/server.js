var express = require('express');
var app = express();
var _ = require('underscore');

var GitHubApi = require('github');

var github = new GitHubApi({
  Promise: require('bluebird'),
  debug: false
});

app.get('/', function(req, res) {
  res.send('Hello World!');
});

app.get('/search', function(req, res) {
  github.authenticate({
    type: 'oauth',
    token: 'd69849cc2cb318413e36fd34a06b905091078cba'
  });

  // fetch NodeJS repository
  github.search.repos({
    q: 'node',
    user: 'nodejs'
  }).then(function(results) {
    var result = results.items[0];
    var user = result.owner.login;
    var repo = result.name;

    if (user === 'nodejs' && repo === 'node') {
      // fetch commits
      github.repos.getCommits({
        user: user,
        repo: repo,
        per_page: 40
      }).then(function(results) {
        var items = _.map(results, function(result) {

          // remove unnecessary data and add extra color information
          // based on commit's SHA hash code
          return {
            sha: result.sha,
            author: result.commit.author.name,
            color: isNaN(result.sha.slice(-1)) ?  undefined : '#DC143C'
          };
        });

        // sort commits
        var sortedItems = _.sortBy(items, 'author');
        res.json(sortedItems);
      });
    }
  });
});

app.listen(3000, function() {
  console.log('Example app listening on port 3000!');
});

module.exports.app = app;
